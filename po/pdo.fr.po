# translation of pdo.fr.po to French
# This file is put in the public domain.
# Guilhelm Panaget <guilhelm.panaget@free.fr>
# Brice Favre <bfavre@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: pdo.fr\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-12-11 15:50+0800\n"
"PO-Revision-Date: 2009-04-12 18:27+0200\n"
"Last-Translator: Brice Favre <bfavre@gmail.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: bin/create_index_pages:69
msgid "virtual package provided by"
msgstr "paquet virtuel fourni par"

#: bin/create_index_pages:171 bin/create_index_pages:212
msgid "Section"
msgstr "Section"

#: bin/create_index_pages:178 bin/create_index_pages:220
msgid "Subsection"
msgstr "Sous-section"

#: bin/create_index_pages:185 bin/create_index_pages:228
msgid "Priority"
msgstr "Priorité"

#: lib/Packages/Dispatcher.pm:347
msgid "requested format not available for this document"
msgstr "le format demandé n'est pas disponible pour ce document"

#: lib/Packages/DoDownload.pm:27 lib/Packages/DoFilelist.pm:27
#: lib/Packages/DoShow.pm:31
msgid "package not valid or not specified"
msgstr "paquet invalide ou non précisé"

#: lib/Packages/DoDownload.pm:30 lib/Packages/DoFilelist.pm:30
#: lib/Packages/DoIndex.pm:37 lib/Packages/DoNewPkg.pm:22
#: lib/Packages/DoSearchContents.pm:30 lib/Packages/DoShow.pm:34
msgid "suite not valid or not specified"
msgstr "suite invalide ou non précisée"

#: lib/Packages/DoDownload.pm:33 lib/Packages/DoFilelist.pm:33
msgid "architecture not valid or not specified"
msgstr "architecture invalide ou non précisée"

#: lib/Packages/DoDownload.pm:36
#, perl-format
msgid "more than one suite specified for download (%s)"
msgstr "plus d'une suite spécifiée pour télécharger (%s) "

#: lib/Packages/DoDownload.pm:40
#, perl-format
msgid "more than one architecture specified for download (%s)"
msgstr "plus d'une architecture spécifiée pour télécharger (%s) "

#: lib/Packages/DoDownload.pm:63 lib/Packages/DoShow.pm:73
msgid "No such package."
msgstr "Pas de paquet"

#: lib/Packages/DoDownload.pm:90
msgid "kByte"
msgstr ""

#: lib/Packages/DoDownload.pm:93
msgid "MByte"
msgstr ""

#: lib/Packages/DoFilelist.pm:48
msgid "No such package in this suite on this architecture."
msgstr "Aucun paquet de cette suite au sein de cette architecture."

#: lib/Packages/DoFilelist.pm:60
msgid "Invalid suite/architecture combination"
msgstr "Combinaison suite/architecture invalide"

#: lib/Packages/DoIndex.pm:40
#, perl-format
msgid "more than one suite specified for show_static (%s)"
msgstr "plus d'une suite spécifiée pour show_static (%s)"

#: lib/Packages/DoIndex.pm:44
#, perl-format
msgid "more than one subsection specified for show_static (%s)"
msgstr "plus d'une sous-section pour show_static (%s)"

#: lib/Packages/DoIndex.pm:81
#, perl-format
msgid "couldn't read index file %s: %s"
msgstr "impossible de lire le fichier d'index %s : %s"

#: lib/Packages/DoNewPkg.pm:25
#, perl-format
msgid "more than one suite specified for newpkg (%s)"
msgstr "plus d'une suite spécifiée pour newpkg (%s)"

#: lib/Packages/DoNewPkg.pm:43
#, perl-format
msgid "no newpkg information found for suite %s"
msgstr "pas d'informations newpkg trouvées pour la suite %s"

#: lib/Packages/DoSearch.pm:25 lib/Packages/DoSearchContents.pm:24
msgid "keyword not valid or missing"
msgstr "mot clé invalide ou manquant"

#: lib/Packages/DoSearch.pm:28 lib/Packages/DoSearchContents.pm:27
msgid "keyword too short (keywords need to have at least two characters)"
msgstr ""
"mot clé trop court (les mots clés doivent comporter au moins deux caractères)"

#: lib/Packages/DoSearch.pm:169
msgid "Exact hits"
msgstr "Résultats exacts"

#: lib/Packages/DoSearch.pm:179
msgid "Other hits"
msgstr "Autres résultats"

#: lib/Packages/DoSearch.pm:238
msgid "Virtual package"
msgstr "Paquet virtuel"

#: lib/Packages/DoSearchContents.pm:40
#, perl-format
msgid "more than one suite specified for contents search (%s)"
msgstr "plus d'une suite spécifiée pour la recherche de contenus (%s)"

#: lib/Packages/DoSearchContents.pm:62
msgid "No contents information available for this suite"
msgstr "Pas d'informations de contenus disponibles pour cette suite"

#: lib/Packages/DoSearchContents.pm:86
msgid "The search mode you selected doesn't support more than one keyword."
msgstr ""
"Le mode de recherche que vous avez sélectionné ne supporte pas plus d'un mot "
"clé."

#: lib/Packages/DoShow.pm:37
#, perl-format
msgid "more than one suite specified for show (%s)"
msgstr "plus d'une suite spécifiée pour afficher (%s)"

#: lib/Packages/DoShow.pm:85
msgid "Package not available in this suite."
msgstr "Paquet indisponible dans cette suite"

#: lib/Packages/DoShow.pm:198
msgid " and others"
msgstr " et autres"

#: lib/Packages/DoShow.pm:254
#, fuzzy
#| msgid "Virtual package"
msgid "virtual package"
msgstr "Paquet virtuel"

#: lib/Packages/DoShow.pm:435
#, perl-format
msgid "not %s"
msgstr "non %s"

#: lib/Packages/DoShow.pm:485
msgid "Package not available"
msgstr "Paquet indisponible"

#: lib/Packages/DoShow.pm:524
msgid "Not available"
msgstr "Indisponible"

#: lib/Packages/Page.pm:47
msgid "package has bad maintainer field"
msgstr "paquet doté d'un champ de responsable incorrect"
